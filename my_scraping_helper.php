<?php
function filter_category()
{
	$filter_cate = array(
			"ao"                    => "thoi-trang"
			, "ao-khoac"            => "thoi-trang"
			, "ao-tam"              => "thoi-trang"
			, "ao-thun"             => "thoi-trang"
			, "ao-ren"              => "thoi-trang"
			, "ao-so-mi"            => "thoi-trang"
			, "ao-khoac"            => "thoi-trang"
			, "ao-nam"              => "thoi-trang"
			, "chan-vay"            => "thoi-trang"
			, "day-nit"             => "thoi-trang"
			, "thoi-trang"          => "thoi-trang"
			, "quan-lot"            => "thoi-trang"
			, "quan-dai"            => "thoi-trang"
			, "quan-ao"             => "thoi-trang"
			, "tui"                 => "thoi-trang"
			, "tui-xach"            => "thoi-trang"
			, "tui-hop"             => "thoi-trang"
			, "ba-lo"               => "thoi-trang"
			, "vi-cam-tay"          => "thoi-trang"
			, "dam-thoi-trang"      => "thoi-trang"
			, "dam-voan"            => "thoi-trang"
			, "balo"                => "thoi-trang"
			, "vay"                 => "thoi-trang"
			, "mat-kinh"            => "thoi-trang"
			, "vest"                => "thoi-trang"
			, "bo-do"               => "thoi-trang"
			, "dam-da-tiec"         => "thoi-trang"
			, "dam-dao-pho"         => "thoi-trang"
			, "dam-bo-eo"           => "thoi-trang"
			, "dam-cong-so"         => "thoi-trang"
			, "dam-ren"             => "thoi-trang"
			, "dam-xuong"           => "thoi-trang"
			, "dam-duoi-ca"         => "thoi-trang"
			, "set-do-bo"           => "thoi-trang"
			, "do-doi-nu"           => "thoi-trang"
			, "noi-y"               => "thoi-trang"
			, "quan-nam"            => "thoi-trang"
			, "quan-nu"             => "thoi-trang"
			, "bo-do-the-thao-nam"  => "thoi-trang"
			, "do-doi-nam"          => "thoi-trang"
			, "jumpsuit"            => "thoi-trang"
			, "dam-yem"             => "thoi-trang"
			, "bo-do"               => "thoi-trang"
			, "giay"                => "thoi-trang"
			, "dam"                 => "thoi-trang"
			, "quan-jeans"          => "thoi-trang"
			, "set-bo"              => "thoi-trang"
			, "me-be"               => "thoi-trang"
			, "day-chuyen"          => "thoi-trang"
			, "bikini"              => "thoi-trang"
			, "dong-ho"             => "cong-nghe"
			, "chuot"               => "cong-nghe"
			, "ban-phim"            => "cong-nghe"
			, "day-cap"             => "cong-nghe"
			, "the-nho"             => "cong-nghe"
			, "vi-tinh"             => "cong-nghe"
			, "lcd"                 => "cong-nghe"
			, "usb"                 => "cong-nghe"
			, "dvd"                 => "cong-nghe"
			, "cd"                  => "cong-nghe"
			, "tai-nghe"            => "cong-nghe"
			, "pin-sac"             => "cong-nghe"
			, "coc-sac"             => "cong-nghe"
			, "but-trinh-chieu"     => "cong-nghe"
			, "wifi"                => "cong-nghe"
			, "pin"                 => "cong-nghe"
			, "camera"              =>"cong-nghe"
			," dien-thoai"          =>"cong-nghe"
			, "tri-lieu"            => "cong-nghe"
			, "day-sac"             => "cong-nghe"
			, "laptop"              => "cong-nghe"
			, "gay-chup-hinh"       => "cong-nghe"
			, "bo-sac"              => "cong-nghe"
			, "am-thanh"            => "cong-nghe"
			, "tivi"                => "cong-nghe"
			, "audio"               => "cong-nghe"
			, "video"               => "cong-nghe"
			, "op-lung"             => "cong-nghe"
			, "bao-da"              => "cong-nghe"
			, "may-tinh-bang"       => "cong-nghe"
			, "hdmi"                => "cong-nghe"
			, "iphone"              => "cong-nghe"
			, "thiet-bi-android"    => "cong-nghe"
			, "dau-doc-the"         => "cong-nghe"
			, "du-lich"             => "du-lich"
			, "tour"                => "du-lich"
			, "nghi-duong"          => "du-lich"
			, "khach-san"           => "du-lich"
			, "nha-hang"            => "du-lich"
			, "resort"              => "du-lich"
			, "holiday"             => "du-lich"
			, "buffet"              => "am-thuc"
			, "an-uong"             => "am-thuc"
			, "may-xay-sinh-to"     => "gia-dung-va-noi-that"
			, "cay-vot-muoi"        => "gia-dung-va-noi-that"
			, "thot-go"             => "gia-dung-va-noi-that"
			, "hop-dung-com"        => "gia-dung-va-noi-that"
			, "hop-com"             => "gia-dung-va-noi-that"
			, "tam-tham"            => "gia-dung-va-noi-that"
			, "noi-dat"             => "gia-dung-va-noi-that"
			, "dung-cu"             => "gia-dung-va-noi-that"
			, "noi-ap-suat"         => "gia-dung-va-noi-that"
			, "chao-chong-dinh"     => "gia-dung-va-noi-that"
			, "hop-nhua"            => "gia-dung-va-noi-that"
			, "am-dun-nuoc"         => "gia-dung-va-noi-that"
			, "gas"                 => "gia-dung-va-noi-that"
			, "noi"                 => "gia-dung-va-noi-that"
			, "may-ep"              => "gia-dung-va-noi-that"
			, "lo-nuong"            => "gia-dung-va-noi-that"
			, "lo-vi-song"          => "gia-dung-va-noi-that"
			, "quat"                => "gia-dung-va-noi-that"
			, "quat-may"            => "gia-dung-va-noi-that"
			, "quat-sac"            => "gia-dung-va-noi-that"
			, "may-hut-bui"         => "gia-dung-va-noi-that"
			, "ban-ui"              => "gia-dung-va-noi-that"
			, "voi-tam-hoa-sen"     => "gia-dung-va-noi-that"
			, "electronic"          => "gia-dung-va-noi-that"
			, "den-led"             => "gia-dung-va-noi-that"
			, "bon-cau"             => "gia-dung-va-noi-that"
			, "tre-em"              => "me-va-be"
			, "me-va-be"            => "me-va-be"
			, "dam-me-be"           => "me-va-be"
			, "but-chi-mau"         => "me-va-be"
			, "bo-mau-ve"           => "me-va-be"
			, "hop-mau"             => "me-va-be"
			, "nuoc-hoa"            => "spa-va-lam-dep"
			, "my-pham"             => "spa-va-lam-dep"
			, "kem-tay"             => "spa-va-lam-dep"
			, "sua-tam"             => "spa-va-lam-dep"
			, "kem-duong"           => "spa-va-lam-dep"
			, "wax"                 => "spa-va-lam-dep"
			, "mat-na"              => "spa-va-lam-dep"
			, "massage"             => "spa-va-lam-dep"
			, "trang-diem"          => "spa-va-lam-dep"
			, "nhu-hoa"             => "spa-va-lam-dep"
			, "bo-kem-cat-mong-tay" => "spa-va-lam-dep"
			, "may-hut-mun"         => "spa-va-lam-dep"
			, "dung-cu-lam-mong"    => "spa-va-lam-dep"
			, "nang-nguc"           => "spa-va-lam-dep"
			, "triet-long"          => "spa-va-lam-dep"
			, "hap-trang"           => "spa-va-lam-dep"
			, "kem-tri-tham"        => "spa-va-lam-dep"
			, "chong-nang"          => "spa-va-lam-dep"
			, "may-cao-rau"         => "khac"
			, "bo-giac-hoi"         => "khac"
			, "tong-do"             => "khac"
			, "luoi-dem"            => "khac"
			, "bo-voi-sen"          => "khac"
			, "cat-mau"             => "khac"
			, "goi"                 => "khac"
			, "hot-quet-bat-lua"    => "khac"
			, "quat"                => "khac"
			, "bo-co-tuong"         => "khac");
		return $filter_cate;
	}

	/*
	 * Reference:
	 * 1. http://truongkienthuc.vn/threads/danh-sach-64-tinh-thanh-va-ma-vung.216/
	 * 2. https://sites.google.com/site/provincemap/mienbac
	 */
	function areaClearing( $prefecture_name){
		$area_arr_north   = array( "Hà Nội","Bắc Giang", "Bắc Kạn", "Bắc Ninh", "Cao Bằng", "Hà Giang", "Hà Nam", "Hải Dương", "Hưng Yên", "Hải Phòng", "Lai Châu", "Lạng Sơn", "Lào Cai", "Nam Định", "Ninh Bình", "Phú Thọ", "Quảng Ninh", "Sơn La", "Thái Bình", "Thái Nguyên", "Tuyên Quang", "Vĩnh Phúc", "Yên Bái", "Điện Biên","Thanh Hoá","Thanh Hóa" );
		$area_arr_central = array( "Đà Nẵng", "Bình Định", "Dak Lak", "Đăk Lăk", "Dak Nông", "Gia Lai","Bình Thuận", "Hà Tĩnh", "Hòa Bình", "Khánh Hòa", "Khánh Hoà","Hà Tây", "Lâm Đồng", "Kom Tum", "Ninh Thuận", "Nghệ An", "Phú Yên", "Quảng Bình", "Quảng Nam", "Quảng Ngãi", "Quảng Trị", "Thừa Thiên Huế", "Thừa Thiên - Huế" );
		$area_arr_south   = array( "Hồ Chí Minh", "An Giang", "Hậu Giang", "Bà Rịa - Vũng Tàu", "Bà Rịa Vũng Tàu", "Bạc Liêu", "Bến Tre", "Bình Dương", "Bình Phước",  "Cà Mau", "Cần Thơ", "Đồng Nai", "Đồng Tháp", "Kiên Giang",  "Long An",  "Sóc Trăng", "Tây Ninh", "Tiền Giang", "Trà Vinh", "Vĩnh Long" );

		$area_arr = array(
						'bac-bo'   => $area_arr_north,
						'trung-bo' => $area_arr_central,
						'nam-bo'   => $area_arr_south
				     );

		$area_name = trim(ucwords($prefecture_name));
		$area_name = str_replace("  ", "", $area_name);
		$area_name = str_replace(array("Thủ đô ", "Tp ", "Tp.", "Thành phố ", "Thành Phố ","Tỉnh","TP ","TP."), "", $area_name);
		$area_name = trim($area_name);

		$area_exist_flag = 0; //Unknown
		foreach ($area_arr as $key => $area) {
			if( in_array($area_name , $area) ){
				$area_exist_flag = 1; //Exist
				$area_code = $key;
				break;
			}
		}
		if($area_exist_flag == 0) $area_code = 'nam-bo';
 		return  $area_code;
	}

	/*
	 * filter from prefecture/city, and get correct name/code.
	 * Purpose: to insert correct code into DB Bà Rịa Vũng Tàu
	 */
	function prefectureClearing($prefecture_name) {
		$pref_arr = array(
							'an-giang'        => 'An Giang',
							'ba-ria-vung-tau' => 'Bà Rịa Vũng Tàu',
							'ba-ria-vung-tau' => 'Bà Rịa - Vũng Tàu',
							'bac-giang'       => 'Bắc Giang',
							'bac-can'         => 'Bắc Cạn',
							'bac-lieu'        => 'Bạc Liêu',
							'bac-ninh'        => 'Bắc Ninh',
							'ben-tre'         => 'Bến Tre',
							'binh-dinh'       => 'Bình Định',
							'binh-duong'      => 'Bình Dương',
							'binh-phuoc'      => 'Bình Phước',
							'binh-thuan'      => 'Bình Thuận',
							'ca-mau'          => 'Cà Mau',
							'can-tho'         => 'Cần Thơ',
							'cao-bang'        => 'Cao Bằng',
							'da-nang'         => 'Đà Nẵng',
							'dak-lak'         => 'Dak Lak',
							'dak-lak'         => 'Đăk Lăk',
							'dak-nong'        => 'Dak Nông',
							'dien-bien'       => 'Điện Biên',
							'dong-nai'        => 'Đồng Nai',
							'dong-thap'       => 'Đồng Tháp',
							'gia-lai'         => 'Gia Lai',
							'ha-giang'        => 'Hà Giang',
							'ha-nam'          => 'Hà Nam',
							'ha-noi'          => 'Hà Nội',
							'ha-tinh'         => 'Hà Tĩnh',
							'ha-tay'          => 'Hà Tây',
							'hai-duong'       => 'Hải Dương',
							'hai-phong'       => 'Hải Phòng',
							'hau-giang'       => 'Hậu Giang',
							'ho-chi-minh'     => 'Hồ Chí Minh',
							'hoa-binh'        => 'Hòa Bình',
							'hung-yen'        => 'Hưng Yên',
							'khanh-hoa'       => 'Khánh Hoà',
							'khanh-hoa'       => 'Khánh Hòa',
							'kien-giang'      => 'Kiên Giang',
							'kon-tum'         => 'Kom Tum',
							'lai-chau'        => 'Lai Châu',
							'lam-dong'        => 'Lâm Đồng',
							'lang-son'        => 'Lạng Sơn',
							'lao-cai'         => 'Lào Cai',
							'long-an'         => 'Long An',
							'nam-dinh'        => 'Nam Định',
							'nghe-an'         => 'Nghệ An',
							'ninh-binh'       => 'Ninh Bình',
							'ninh-thuan'      => 'Ninh Thuận',
							'phu-tho'         => 'Phú Thọ',
							'phu-yen'         => 'Phú Yên',
							'quang-binh'      => 'Quảng Bình',
							'quang-nam'       => 'Quảng Nam',
							'quang-ngai'      => 'Quảng Ngãi',
							'quang-ninh'      => 'Quảng Ninh',
							'quang-tri'       => 'Quảng Trị',
							'soc-trang'       => 'Sóc Trăng',
							'son-la'          => 'Sơn La',
							'tay-ninh'        => 'Tây Ninh',
							'thai-binh'       => 'Thái Bình',
							'thai-nguyen'     => 'Thái Nguyên',
							'thanh-hoa'       => 'Thanh Hóa',
							'thua-thien-hue'  => 'Thừa Thiên Huế',
							'thua-thien-hue'  => 'Thừa Thiên - Huế',
							'tien-giang'      => 'Tiền Giang',
							'tra-vinh'        => 'Trà Vinh',
							'tuyen-quang'     => 'Tuyên Quang',
							'vinh-long'       => 'Vĩnh Long',
							'vinh-phuc'       => 'Vĩnh Phúc',
							'yen-bai'         => 'Yên Bái'
		);

		$prefecture_name = trim(ucwords($prefecture_name));
		$prefecture_name = str_replace("  ", "", $prefecture_name);
		$prefecture_name = str_replace(array("Thủ đô ", "Tp ", "Tp. ", "Thành phố ", "Thành Phố ", "Tỉnh", "TP ", "TP.", ), "", $prefecture_name);
		$prefecture_name = trim($prefecture_name);

		if ( !in_array($prefecture_name , $pref_arr) ) {
			$salon_pref = 'ho-chi-minh';
		}

		$pref_flip_arr = array_flip($pref_arr);
		$salon_pref = $pref_flip_arr[$prefecture_name];
		return  $salon_pref;
	}

	function geoCode($full_address){
		$google_maps_url = "http://maps.google.com/maps/api/geocode/json?address=".urlencode($full_address)."&sensor=false";
		echo "&gt;&gt; <a href='$google_maps_url'>Google Maps</a>: " . $google_maps_url . "<br>";
		$json = @file_get_contents($google_maps_url);
		$geo = json_decode($json, true);
		if($geo['status']="OK"){
			return $geo;
		}
		return false;
	}

	function savePhoto($arr_imgs,$folder_name,$title){
		if(!$arr_imgs){return false;}

		//remove_all_file_in_folder(dirname(__FILE__)."/../../uploads/images/".$folder_name); dd(111);


		if(folder_exist(dirname(__FILE__)."/../../uploads/images/".$folder_name)==false){
			@mkdir(dirname(__FILE__)."/../../uploads/images/".$folder_name, 0777, true);
		}

		if(folder_exist(dirname(__FILE__)."/../../uploads/images/".$folder_name."/". date("Ym"))==false){
			@mkdir(dirname(__FILE__)."/../../uploads/images/".$folder_name."/". date("Ym"), 0777, true);
		}

		if(is_array($arr_imgs) && count($arr_imgs) > 1){

			preg_match('/<img(.*?)src=\"(.*?)\"(.*?)>/is', $arr_imgs[1], $arr_matches_img_s);
			//d($arr_matches_img_s);
			$path = $arr_matches_img_s[2];

			//$path = preg_replace("/ /","%20",$path[1]);
			//save more image
			$arr_path = $arr_imgs;
			foreach ($arr_path as $path_key => $path_value) {
				/*preg_match("/([^\.]+)$/", $path_value, $arr_match[$path_key]);
				if( ! preg_match("/(jpeg|png|jpg|gif)$/", $arr_match[$path_key][1]) ){
						$arr_match[$path_key][1]="jpeg";
				}*/
				preg_match('/<img(.*?)src=\"(.*?)\"(.*?)>/is', $path_value, $arr_matches_img_s);
				//d($arr_matches_img_s);
				$path_url = $arr_matches_img_s[2];
				$f_extension = f_extension($path_url);

				$data[$path_key]["coupon_photo"] = "/uploads/images/".$folder_name."/". date("Ym") ."/".$title."_".md5($path_key)."_".$path_key.".{$f_extension}";
				if(!file_exists($data[$path_key]["coupon_photo"])){
					$buf = @file_get_contents($path_url);
					if(!$buf){
						$buf = @file_get_contents($path_url);
					}
					@file_put_contents(dirname(__FILE__)."/../..".$data[$path_key]["coupon_photo"],$buf);
				}
			}
			//end save more image

		}else{
			preg_match('/<img(.*?)src=\"(.*?)\"(.*?)>/is', $arr_imgs[0], $arr_matches_img_s);
			$path = $arr_matches_img_s[2];

			//$path = preg_replace("/ /","%20",$arr_imgs);
		}
		$f_extension = f_extension($path);

		/*$path_ = !is_array($path)?$path:$path[0];
		preg_match("/([^\.]+)$/",$path_, $match);*/

		//check image
		/*if( !$match || ! preg_match("/(jpeg|png|jpg|gif)$/", $match[1] ) ){
				$match[1]="jpeg";
		}*/

		$data["coupon_photo"] = "/uploads/images/".$folder_name."/". date("Ym") ."/".$title."_".md5($path).".{$f_extension}";
		if(!file_exists($data["coupon_photo"])){
			//d('save file');
			$buf = @file_get_contents($path);
			if(!$buf){
				$buf = @file_get_contents($path);
			}
			//d('buf: '.$buf);
			//d('photo: '.$data["coupon_photo"]);
			@file_put_contents(dirname(__FILE__)."/../..".$data["coupon_photo"],$buf);
		}
		return $data;
	}
<?php
if (!function_exists('debug_log'))
{
	function debug_log($prefix = 'debug', $php_error = FALSE)
	{			
		$_log =& load_class('Log');
		$ci =& get_instance();
		$message = "corpId ".userdata('corp')['corp_id']." stafId ".userdata('corp')['id']."(".$_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_X_FORWARDED_FOR'].") edit ".$ci->db->last_query();
		
		$_log->write_log('DEBUG', $message, $php_error, $prefix.'-', TRUE);
	}
}

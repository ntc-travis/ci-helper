<?php
function export_to_csv($data,$filename)
{
	$ci =& get_instance();
 	$ci->load->dbutil();
    $ci->load->helper('file');
    $ci->load->helper('download');
	$delimiter = ",";
	$newline   = "\r\n";
	if(!empty($data)){
		$result = $ci->dbutil->csv_from_result($data, $delimiter, $newline);
		force_download($filename, $result);
	}
	return false;

}
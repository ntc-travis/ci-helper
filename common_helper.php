<?php
function is_dev()
{
	if(defined('ENVIRONMENT') && (ENVIRONMENT === 'development' || ENVIRONMENT === 'testing')) {
		return true;
	}
	return false;
}
function is_sp()
{
	$ci =& get_instance();
	$ci->load->library('user_agent');
	if($ci->agent->is_mobile() || $ci->agent->is_smartphone()) {
		return true;
	}

	return false;
}
function conf($item, $file = '')
{
	if($file) {
		get_instance()->config->load($file);
	}
	return get_instance()->config->item($item);
}
function http_response_($url)
{
	$headers = get_headers($url);
	return substr($headers[0], 9, 3);
}
function http_response($url)
{
	/*
	return array
				0 	HTTP/1.1 200 OK
				1 	Date: Wed, 30 Mar 2016 06:13:54 GMT
				2 	Server: Apache/2.4.18 (Win32) OpenSSL/1.0.2e PHP/7.0.1
				3 	Last-Modified: Sun, 27 Mar 2016 11:42:46 GMT
				4 	ETag: "72d3b-52f064b77b980"
				5 	Accept-Ranges: bytes
				6 	Content-Length: 470331
				7 	Connection: close
				8 	Content-Type: image/jpeg
	 */
	$headers = get_headers($url);
	$head = array();
    foreach( $headers as $k=>$v )
    {
        $t = explode( ':', $v, 2 );
        if( isset( $t[1] ) )
            $head[ trim($t[0]) ] = trim( $t[1] );
        else
        {
            $head[] = $v;
            if( preg_match( "#HTTP/[0-9\.]+\s+([0-9]+)#",$v, $out ) )
                $head['reponse_code'] = intval($out[1]);
        }
    }
    return $head;
}
function assets_url() {
    return base_url();
}
/*domain:  http://www.css-tricks.com*/
/*function is_site($domain)
{
   //check, if a valid url is provided
   if(!filter_var($domain, FILTER_VALIDATE_URL)){return false;}

   //initialize curl
   $curlInit = curl_init($domain);
   curl_setopt($curlInit,CURLOPT_CONNECTTIMEOUT,10);
   curl_setopt($curlInit,CURLOPT_HEADER,true);
   curl_setopt($curlInit,CURLOPT_NOBODY,true);
   curl_setopt($curlInit,CURLOPT_RETURNTRANSFER,true);

   //get answer
   $response = curl_exec($curlInit);

   curl_close($curlInit);

   if ($response) return true;

   return false;
}*/
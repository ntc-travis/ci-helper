<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*@Description : remove font unicode and special character
* @param $str string
* @param $format  MB_CASE_UPPER --> CỘNG HÒA
*                 MB_CASE_TITLE --> Cộng Hòa
*                 MB_CASE_LOWER --> cộng hòa
* @return  string
 */

function change_title($str)
{
     if(!$str) return false;
    $unicode = array(
        'a'=>array('á','à','ả','ã','ạ','ă','ắ','ặ','ằ','ẳ','ẵ','â','ấ','ầ','ẩ','ẫ','ậ'),
        'A'=>array('Á','À','Ả','Ã','Ạ','Ă','Ắ','Ặ','Ằ','Ẳ','Ẵ','Â','Ấ','Ầ','Ẩ','Ẫ','Ậ'),
        'd'=>array('đ'),
        'D'=>array('Đ'),
        'e'=>array('é','è','ẻ','ẽ','ẹ','ê','ế','ề','ể','ễ','ệ'),
        'E'=>array('É','È','Ẻ','Ẽ','Ẹ','Ê','Ế','Ề','Ể','Ễ','Ệ'),
        'i'=>array('í','ì','ỉ','ĩ','ị','ị'),
        'I'=>array('Í','Ì','Ỉ','Ĩ','Ị'),
        'o'=>array('ó','ò','ỏ','õ','ọ','ô','ố','ồ','ổ','ỗ','ộ','ơ','ớ','ờ','ở','ỡ','ợ','ó','ọ'),
        'O'=>array('Ó','Ò','Ỏ','Õ','Ọ','Ô','Ố','Ồ','Ổ','Ỗ','Ộ','Ơ','Ớ','Ờ','Ở','Ỡ','Ợ'),
        'u'=>array('ú','ù','ủ','ũ','ụ','ư','ứ','ừ','ử','ữ','ự'),
        'U'=>array('Ú','Ù','Ủ','Ũ','Ụ','Ư','Ứ','Ừ','Ử','Ữ','Ự'),
        'y'=>array('ý','ỳ','ỷ','ỹ','ỵ'),
        'Y'=>array('Ý','Ỳ','Ỷ','Ỹ','Ỵ'),
        '-'=>array(' ','&quot;','.','-–-')
    );
    foreach($unicode as $nonUnicode=>$uni){
        foreach($uni as $value)
        $str = @str_replace($value,$nonUnicode,$str);
        $str = preg_replace("/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/","-",$str);
        $str = preg_replace("/-+-/","-",$str);
        $str = preg_replace("/^\-+|\-+$/","",$str);
    }
    return strtolower($str);
}//END


function change_title_($str,$format='')
{
	$str = strip_unicode($str);
	$str = str_replace("?","",$str);
	$str = str_replace("&","",$str);
	$str = str_replace("'","",$str);
	$str = str_replace("  "," ",$str);
	$str = str_replace(" ''","",$str);
	$str = str_replace("'' ","",$str);
	$str = str_replace("/","",$str);
	$str = str_replace(" // ","",$str);
	$str = str_replace(" \\ ","",$str);
	$str = str_replace("(","",$str);
	$str = str_replace(")","",$str);
	$str = str_replace("[","",$str);
	$str = str_replace("]","",$str);
	$str = str_replace("{","",$str);
	$str = str_replace("}","",$str);
	$str = str_replace(":","",$str);
	$str = str_replace(";","",$str);
	$str = str_replace("<","",$str);
	$str = str_replace(">","",$str);
	$str = str_replace(",","",$str);
	$str = str_replace(".","",$str);
	$str = str_replace("@","",$str);
	$str = str_replace("!","",$str);
	$str = str_replace("*","",$str);
	$str = str_replace("#","",$str);
	$str = str_replace("$","",$str);
	$str = str_replace("%","",$str);
	$str = str_replace("^","",$str);
	//$str = str_replace("-","",$str);
	$str = str_replace("--","-",$str);
	$str = str_replace("-–-","-",$str);
	$str = str_replace("...","",$str);
	$str = str_replace("+","",$str);
	$str = str_replace("=","",$str);
	$str = str_replace("_","",$str);
	$str = str_replace(" – ","",$str);
	$str = str_replace("__","_",$str);
	$str = trim($str);
	if($format!=''){
		$str = mb_convert_case($str , $format , 'utf-8');
	}else{
		$str = mb_convert_case($str , MB_CASE_LOWER , 'utf-8');
	}
	$str = str_replace(" ","-",$str);
	return $str;
}
function strip_unicode($str)
{
	if(!$str){ return false;};
	$unicode = array(
		 'a'=>'á|à|ả|ã|ạ|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ',
		 'A'=>'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ằ|Ẳ|Ẵ|Ặ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
		 'd'=>'đ',
		 'D'=>'Đ',
		 'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
		 'E'=>'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
		 'i'=>'í|ì|ỉ|ĩ|ị',
		 'I'=>'Í|Ì|Ỉ|Ĩ|Ị',
		 'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
		 'O'=>'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
		 'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
		 'U'=>'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
		 'y'=>'ý|ỳ|ỷ|ỹ|ỵ',
		 'Y'=>'Ý|Ỳ|Ỷ|Ỹ|Ỵ'
		);
	foreach($unicode as $key=>$value) {
	  $arr = explode("|",$value);
	  $str = str_replace($arr,$key,$str);
	}
	return $str;
}
function format_str($str,$format='')
{
	if($format!=''){
		$str = mb_convert_case(trim($str) , $format , 'utf-8');
	}else{
		$str = mb_convert_case(trim($str) , MB_CASE_LOWER , 'utf-8');
	}
	return $str;
}

function hp_convert_str($str,$format)
{
	$data ='';
	if($format==''){
		$data = trim(strtolower(mb_convert_encoding($str,"HTML-ENTITIES","UTF-8")));
	}else{
		$data = trim(strtolower(mb_convert_encoding($str,"HTML-ENTITIES",$format)));
	}
	return $data;
}
function split_str_1($str,$limit)
{
	$str = strip_tags($str);
	if(strlen($str)<=$limit)
	{
		return $str;
	}
	else{
		if(strpos($str," ",$limit) > $limit){
			$new_limit=strpos($str," ",$limit);
			$new_str = substr($str,0,$new_limit)." ...";
			return $new_str;
		}
		$new_str = substr($str,0,$limit)." ...";
		return $new_str;
	}
}/*in this time -> show wrong*/
function hp_word_limiter($str, $len)
{
	if (strlen($str) < $len){ return $str; }
	$str = substr($str,0,$len);

	if ($spc_pos = strrpos($str," "))
	{
		$str = substr($str,0,$spc_pos);
	}
	return $str . " ...";
}
function ramdom_string($number)
{
	$values ='';
	$stringlist="ABCDEFGHIJKLMNOPQRSTUVWXYZWabcdefghijklmnopqrstuvwxyzw0123456789";
	for ($i=0; $i < $number; $i++){
		$positions = mt_rand( 0 ,strlen($stringlist) );
		$values .=  substr($stringlist,$positions,1 );
	}
	return $values;
}
function ramdom_number($number)
{
	$values ='';
	$stringlist="0123456789";
	for ($i=0; $i < $number; $i++){
		$positions = mt_rand( 0 ,strlen($stringlist) );
		$values .= substr($stringlist,$positions,1 );
	}
	return $values;
}
function truncate($input_string, $length, $trimmarker = " ..." ){
    $encoding = mb_detect_encoding( $input_string, 'auto', true );
    return mb_strimwidth($input_string, 0, $length, $trimmarker, $encoding);
}
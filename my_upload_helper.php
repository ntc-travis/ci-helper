<?php

function do_upload_h()
	{
		$this->load->library('upload', $this->set_upload_options());  
		if ( ! $this->upload->do_upload())
		{
			$error = array('message'=> 'error','error' => $this->upload->display_errors());
 
			echo json_encode($error);exit;
		}
		else
		{
			//remove file
			$this->rm_file_temp();

			$data = array('upload_data' => $this->upload->data());
			 
			//resize:
	         
	        //watermarking an Image
	        /*$config['wm_text'] = 'Copyright 2006 - John Doe';
			$config['wm_type'] = 'text';
			$config['wm_font_path'] = '../system/fonts/texb.ttf';
			$config['wm_font_size'] = '16';
			$config['wm_font_color'] = '000000';
			$config['wm_vrt_alignment'] = 'bottom';
			$config['wm_hor_alignment'] = 'center';
			$config['wm_padding'] = '20';*/
	        
	       
	        $this->image_lib->initialize($this->set_upload_resize_options($data['upload_data']['full_path']));
 
	        if ( ! $this->image_lib->resize()){
				$this->session->set_flashdata('message', $this->image_lib->display_errors('', ''));
			}

			$_thumb = $data['upload_data']['raw_name'].'_thumb'.$data['upload_data']['file_ext'];//$data['upload_data']['file_name']

	        //save session
			$return_data = array(
					'message'=> 'success',
					'file_name' => $_thumb
			);

			$this->session->set_userdata('file_name',"upload/profiles/".$_thumb);

			unlink($data['upload_data']['full_path']);
			echo json_encode($return_data);exit;
		}
	}
	
	function set_upload_option_h()//$path,
	{   
		$config                  = array();
		$config['upload_path']   = "./upload/profiles";
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']      = '5000';  //5MB
		$config['max_width']     = '3000';
		$config['max_height']    = '2000';
		$config['remove_spaces'] = TRUE;  
		$config['encrypt_name']  = TRUE;	
		$config['overwrite']     = FALSE;
	    return $config;
	}

	function set_upload_resize_option_h($data_full_path)
	{   
		$config = array();
		$config['image_library']  = 'gd2';
		$config['source_image']   = $data_full_path;
		$config['maintain_ratio'] = FALSE;
		$config['remove_spaces']  = TRUE;  
		$config['encrypt_name']   = TRUE;
		$config['create_thumb']   = TRUE;
		$config['thumb_marker']   = '_thumb';
		$config['width']          = 150;
		$config['height']         = 50;
	    return $config;
	}

	function rm_file_temp_h()
	{
		$ss_file_name = $this->session->userdata('file_name'); 
		$file_name_temp_rm = str_replace('../', '', $ss_file_name); 
		unlink($file_name_temp_rm); 
	}

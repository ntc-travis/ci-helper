<?php
function date_to_Ymd($date){
	return date('Y-m-d',strtotime($date));
}
function date_to_day($date){
	return date('d',strtotime($date));
}
function date_to_month($date){
	return date('M',strtotime($date));
}

/*
@param  $date Y-m-d H:i:s
$return H:i:s
*/
function date_to_time_hour($date){
	return date('H',strtotime($date));
}

function date_to_time_minus($date){
	return date('i',strtotime($date));
}


function date_to_time_second($date){
	return date('s',strtotime($date));
}

/**
 * @param $number int 
 * @param $type   string => day weeek month
 * @return Y-m-d
 * @Exam  datetime_add_more_type(2,'month')
 */
function datetime_add_more_type($number,$type)
{
		$date = strtotime(date('Y-m-d'));
    	$date = strtotime("+".$number." ".$type, $date); 
		return date('Y-m-d', $date);
}

/**
 * Date Calculation - Difference in Days between two Dates
 * Return the number of days between the two dates
 *
 * @param  [datime] $later_date [Y-h-d H:i:s]
 * @param  [datime] $earlier_date [Y-h-d H:i:s]
 * @return [int]     [number]
 * 
 * @Exam:  diff_date("2014-05-10","2014-04-01") 
 *   return ==> 39;
 */
 function diff_date ($later_date, $earlier_date)
 {
  return round(abs(strtotime($later_date)-strtotime($earlier_date))/86400);
 }

/**
 * Date Calculation - Difference in Days between two Dates
 * Return the number of days between the two dates
 *
 * @param  [format] $format [%y |  %R%a | %d]
 * @param  [datime] $earlier_date [Y-h-d H:i:s]
 * @param  [datime] $later_date [Y-h-d H:i:s]
 * @return [int]     [number]
 *
 * @Exam:  diff_date_2("%R%a","2014-05-10","2014-04-01") 
 *   return ==> 39;
 */
function diff_date_2($format, $later_date = null, $earlier_date = null)
{

  	if ($earlier_date == "0000-00-00" || empty($earlier_date)) return "";

	$later   = date_create($later_date);
	$earlier = date_create($earlier_date);

	if(!$later || !$earlier) return '';

	$interval = date_diff($earlier, $later);
	
	return round(abs($interval->format($format)));
}


function diff_date_3($date1 ,$date2)
{
 	$date1 = new DateTime($date1 );
    $date2 = new DateTime($date2);
    $interval = $date1->diff($date2);
    $dt  = $interval->y . " years, " . $interval->m." months, ".$interval->d." days ";
    
    return $dt;

 } 


function last_datetime($day,$number,$type)
{	
	$day    = $day?$day:date('Y-m-d');
	$number = $number?$number:1;
	$type   = $type?$type:'day';
	$dt  = date('Y-m-d',strtotime(strftime("%Y-%m-%d",strtotime(date("Y-m-d", strtotime($day)) . " -".$number." ".$type))));
    
    return $dt;
}

function next_datetime($day,$number,$type)
{
	$day    = $day?$day:date('Y-m-d');
	$number = $number?$number:1;
	$type   = $type?$type:'day';
	$dt     =  date('Y-m-d',strtotime(strftime("%Y-%m-%d",strtotime(date("Y-m-d", strtotime($day)) . " +".$number." ".$type))));
    
    return $dt;
 }
 
 
 function list_day_of_week($date) 
 {
	list( $year, $month, $day) = explode("-", $date);
    $wkday = date('l',mktime('0','0','0', $month, $day, $year));

    switch($wkday) {
		case 'Monday': $numDaysToMon    = 0; break;
		case 'Tuesday': $numDaysToMon   = 1; break;
		case 'Wednesday': $numDaysToMon = 2; break;
		case 'Thursday': $numDaysToMon  = 3; break;
		case 'Friday': $numDaysToMon    = 4; break;
		case 'Saturday': $numDaysToMon  = 5; break;
		case 'Sunday': $numDaysToMon    = 6; break;
    }

    $monday = mktime('0','0','0', $month, $day-$numDaysToMon, $year);
    $seconds_in_a_day = 86400;

    for($i=0; $i<7; $i++) {
        $dates[$i] = date('Y-m-d',$monday+($seconds_in_a_day*$i));
    }

    return $dates;
 }
 
 function list_month_of_year(){
	$arr_month = array();
	for ($m=0; $m<=12; $m++) {
       $month = date('M', mktime(0,0,0,$m, 1, date('Y')));
       $arr_month[] = $month;
    }
    return $arr_month;
 }
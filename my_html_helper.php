<?php
function e($param)
{
	return html_escape($param);
}

function dir_name()
{
	preg_match("#^\/([^/]*)#", $_SERVER['REQUEST_URI'], $ar);
	return $ar[1]?$ar[1]:'';
}


function pager_link($total, $conf_name = null, array $param = [])
{
	$ci =& get_instance();
	$ci->load->library('pagination');
	$conf = ($conf_name) ? conf($conf_name, 'pager') : [];
	 
	if($param){
		if($ci->uri->segment(3)){
		 	if($ci->uri->segment(3) == "product_group"){
				if($param['cate'] = check_gets("cate",true) && check_gets("cate",false) != 'filter'){
					$param_c = check_gets("cate",false);
					$param_v = check_gets("cate",true);
				}
		 
				if($param['filter'] = check_gets("filter",true)  && check_gets("filter",false) != 'cate'){
					$param_c = check_gets("filter",false);
					$param_v = check_gets("filter",true);
				}
				$base_url = isset($param["base_uri"]) ? e($param["base_uri"]) : current_url().'?'.$param_c.'='.$param_v;
		 	}else if($ci->uri->segment(3) == "search"){
		 		$base_url = isset($param["base_uri"]) ? e($param["base_uri"]) : current_url();//.'?'.$param['search'];
		 	}else{
		 		$base_url = isset($param["base_uri"]) ? e($param["base_uri"]) : current_url();
		 	}
		 }else{ 
				$base_url = isset($param["base_uri"]) ? e($param["base_uri"]) : current_url();
		 }
	}else{
				$base_url = isset($param["base_uri"]) ? e($param["base_uri"]) : current_url();  
		 } 
	$config = array_merge([
		'base_url'             => $base_url,
		'first_url'            => $base_url,
		'num_links'            => 5,
		'total_rows'           => $total,
		'per_page'             => $ci->config->item('per_page'),
		'use_page_numbers'     => true,
		'page_query_string'    => false,
		'query_string_segment' => 'p',
		'first_link'           => '&lt;&lt;',
		'last_link'            => '&gt;&gt;',
	], $conf, $param);

	if ($ci->uri->segment(1, 0) == "search") {
		$config["page_query_string"] = true;
	}

	$ci->pagination->initialize($config);
	return $ci->pagination->create_links();
}

function paging($total, $conf_name = null, array $param = [])
{
	$ci =& get_instance();
	$ci->load->library('pagination');
	$conf = ($conf_name) ? conf($conf_name, 'pager') : [];
	 if(isset($param["filter"]) && $param['filter']=='new'){
	 	$base_url = isset($param["base_uri"]) ? e($param["base_uri"]) : current_url().'?filter=new';
	 }elseif(isset($param["cate"])){
	 	$base_url = isset($param["base_uri"]) ? e($param["base_uri"]) : current_url().'?cate='.$param['cate'].'&cate_g='.$param['cate_g'];
	 }else{
	 	$base_url = isset($param["base_uri"]) ? e($param["base_uri"]) : current_url();
	 }

	$config = array_merge([
		'base_url'             => $base_url,
		'first_url'            => $base_url,
		'num_links'            => 5,
		'total_rows'           => $total,
		'per_page'             => $ci->config->item('per_page'),
		'use_page_numbers'     => true,
		'page_query_string'    => false,
		'query_string_segment' => 'p',
		'first_link'           => '&lt;&lt;',
		'last_link'            => '&gt;&gt;',
	], $conf, $param);

	if ($ci->uri->segment(1, 0) == "search") {
		$config["page_query_string"] = true;
	}

	$ci->pagination->initialize($config);
	return $ci->pagination->create_links();
}
function param_search()
{
	$ci =& get_instance();
	$param_search = '';
    foreach ($ci->input->get() as $key => $value) {
        $param_search .=  $key.'='.$value.'&';
    }
    return substr_replace($param_search, "", -1);
}

function time_span_combine($start, $end)
{
	if ($start == null && $end == null) return null;
	return preg_replace("/\:00$/","",$start)."～".preg_replace("/\:00$/","",$end);
}

function required()
{
	return '<code>*</code>';
}

function required_front()
{
	return '<span class="require">*</span>';
}

function view_date($format, $date)
{
	if($date && strpos($date,"0000-00-00")===false) {
		return date($format, strtotime($date));
	}

	return '';
}

function submit_status()
{
	$submit = get_instance()->input->post('submit');
	 
	if(is_array($submit)) {
		foreach ($submit as $key => $value) {
			return $key;
		}
	}
	return null;
}

function check_gets($get_name, $show = false)
{
	$ci =& get_instance();
	$arr_get = $ci->input->get();
	$find_key = array_keys($arr_get);
	$get_param ='';

	foreach ($find_key as $key => $value) {
		if($value == $get_name){
			$get_param = $value;
		}
	}  
	if($show === true){
		return $ci->input->get($get_param);
	}
  	return $get_param;
}

function check_cookie($cookie,$value) 
{
	$ci =& get_instance();
	if($ci->input->cookie($cookie) && $ci->input->cookie($cookie) == $value) {
		return true;
	}
	return false;
}

function hp_price_format ($price,$number,$format1,$format2) 
{
	if($price) {
		return number_format($price,$number,$format1,$format2);
	}else {
		return '0';
	}
}

/*function hp_datetime_format($d)
{
	
	if(preg_match('#^([0-9]{4})[/-]([0-9]{2})[/-]([0-9]{2})$#', $d, $match)) { 
		return $match[1].'年'.$match[2].'月 ( 築'.$match[3].'年 )';
	}
	return '0000年00月 ( 築00年 )';
}*/

/*function hp_datetime_format_sentmail($d)
{
	
	if(preg_match('#^([0-9]{4})[/-]([0-9]{2})[/-]([0-9]{2})[\s]([0-9]{2})[/:]([0-9]{2})[/:]([0-9]{2})$#', $d, $match)) { 
		return $match[1].'年'.$match[2].'月'.$match[3].'日 '.$match[4].'時'.$match[5].'分'.$match[6].'秒';
	}
	return '0000年00月00日 00時00分00秒';
}*/

function str_limit($value, $limit = 100, $end = '...') {
	if(!$value) return '';

	$str  = mb_substr($value, 0, $limit);

	if($str == $value) {
		return $str;
	}

	$str .= $end;
	return $str;
}

function hp_datetime_format_contact($d)
{
	
	if(preg_match('#^([0-9]{4})[/-]([0-9]{2})[/-]([0-9]{2})[\s]([0-9]{2})[/:]([0-9]{2})[/:]([0-9]{2})$#', $d, $match)) { 
		return $match[1].'/'.$match[2].'/'.$match[3].' '.$match[4].':'.$match[5].':'.$match[6];
	}
	return '0000/00/00 00:00:00';
}

function hp_year_options($year)
{
	$return_option = ["" => ""];
	$cur_year = date("Y");
	$val = $cur_year - $year;

	for ($i=0; $i<$year * 2; $i++)
	{
		$val++;
		$return_option += [$val => $val];
	}
	return $return_option;
}

function hp_num_options($number)
{
	$return_option = ["00" => "00"];
	$val = '';
	for ($i=0; $i<$number * 2; $i++)
	{
		$val++;
		$return_option += [str_pad($val-1, 2, "0", STR_PAD_LEFT) => str_pad($val-1, 2, "0", STR_PAD_LEFT)];
	}
	return $return_option;
}

function gender ($sex){
	if ($sex == '1'){ //Male
		return 'Nam';
	}
	if ($sex == '2'){
		return 'Nữ'; //Female
	}
	return '';
}


function news_info_title ($param)
{
	switch ($param) {
		case 'info_active':
			return 'Danh sach tin đang hiển thị';
			break;
		case 'info_unactive':
			return 'Danh sach tin đang ẩn';
			break;
		case 'info_block':
			return 'Danh sach tin bị khoá';
			break;
		case 'info_view':
			return 'Chi tiết tin';
			break;
		
		default:
			return 'Danh sach tin đang hiển thị';
			break;
	}
}

function member_level_title ($level)
{
	switch ($level) {
		case $level <5:
			return 'Thành viên';
			break;		
		case $level <8:
			return 'Khách hàng';
			break;		

		default:
			return 'Quản trị viên';
			break;
	}
}

function member_status ($status)
{
	switch ($status) {
		case $status == 0:
			return 'Chưa kích hoạt';
			break;		
		case $status == 1:
			return 'Đang hoạt động';
			break;	
		case $status == 2:
			return 'Bị khoá';
			break;	
		default:
			return 'Chưa kích hoạt';
			break;
	}
}
<?php

function count_item_in_folder($dir){
        $files = array();
        $directory = opendir($dir);
        while($item = readdir($directory)){
             if(($item != ".") && ($item != "..") && ($item != ".svn") ){
                  $files[] = $item;
             }
        }
        $numFiles = count($files);
        return $numFiles;
    }


 function get_directory_tree($dir)
 {
    $dirs = array_diff( scandir( $dir ), array( ".", ".." ) );
    $dir_array = array();
    foreach( $dirs as $d ){
        if( is_dir($dir."/".$d) ) $dir_array[ $d ] = get_directory_tree( $dir."/".$d );
        else $dir_array[ $d ] = $d;
    }
    return $dir_array;
}

function remove_all_file_in_folder($dir) {
    if ($handle = opendir("$dir")) {
        while (false !== ($item = readdir($handle))) {
            if ($item != "." && $item != "..") {
                if (is_dir("$dir/$item")) {
                    remove_all_file_in_folder("$dir/$item");
                } else {
	                unlink("$dir/$item");
	                //echo " removing $dir/$item<br>\n";
                }
            }
        }
        delete_folder($dir);
        closedir($handle);
    }
}

function delete_folder($path)
{
    if (is_dir($path) === true)
    {
        $files = array_diff(scandir($path), array('.', '..'));

        foreach ($files as $file)
        {
            delete_folder(realpath($path) . '/' . $file);
        }

        return rmdir($path);
    }

    else if (is_file($path) === true)
    {
        return unlink($path);
    }

    return false;
}

function get_files($root_dir, $all_data=array())
{
	// only include files with these extensions
	$allow_extensions = array("php", "html","css",'txt','js');

	// make any specific files you wish to be excluded
	$ignore_files = array("gdform.php","fishheader.php","fishfooter.php",
			          "sitelinks.php","google204accd1c3ac0501.html","sitemapxml.php",
			          "rotate.php", "fishstockingreport2.php", "repcentral.php", "repne.php",
			          "repnorth.php","reppowell.php","repse.php","repsouth.php","repse.php",
			          "stockreplib.php","iestyles.php",'New Text Document (3).txt');
	$ignore_regex = '/^_/';

	// skip these directories
	$ignore_dirs = array(".", "..", "images", "dev", "lib", "data", "osh", "fiq", "google",
	  					"stats", "_db_backups", "maps", "php_uploads", "test");

	// run through content of root directory
	$dir_content = scandir($root_dir);
	foreach($dir_content as $key => $content)
	{
	  $path = $root_dir.'/'.$content;
	  if(is_file($path) && is_readable($path))
	  {
	    // skip ignored files
	    if(!in_array($content, $ignore_files))
	    {
	      if (preg_match($ignore_regex,$content) == 0)
	      {
	        $content_chunks = explode(".",$content);
	        $ext = $content_chunks[count($content_chunks) - 1];
	        // only include files with desired extensions
	        if (in_array($ext, $allow_extensions))
	        {
	            // save file name with path
	            $all_data[] = $path;
	        }
	      }
	    }
	  }
	  // if content is a directory and readable, add path and name
	  elseif(is_dir($path) && is_readable($path))
	  {
	    // skip any ignored dirs
	    if(!in_array($content, $ignore_dirs))
	    {
	      // recursive callback to open new directory
	      $all_data = get_files($path, $all_data);
	    }
	  }
	} // end foreach
	return $all_data;
} 



function scan_directories_1($rootDir, $allowext, $allData=array())
{
	$dirContent = scandir($rootDir);
	foreach($dirContent as $key => $content) {
	    $path = $rootDir.'/'.$content;
	    $ext = substr($content, strrpos($content, '.') + 1);
	    if(in_array($ext, $allowext)) {
	        if(is_file($path) && is_readable($path)) {
	            $allData[] = $path;
	        }elseif(is_dir($path) && is_readable($path)) {
	            // recursive callback to open new directory
	            $allData = scanDirectories_1($path, $allData);
	        }
	    }
	}
	return $allData;
}

function scan_directories_2($rootDir, $allData=array())
{
    // set filenames invisible if you want
    $invisibleFileNames = array(".", "..", ".htaccess", ".htpasswd");
    // run through content of root directory
    $dirContent = scandir($rootDir);
    foreach($dirContent as $key => $content) {
        // filter all files not accessible
        $path = $rootDir.'/'.$content;
        if(!in_array($content, $invisibleFileNames)) {
            // if content is file & readable, add to array
            if(is_file($path) && is_readable($path)) {
                // save file name with path
                $allData[] = $path;
            // if content is a directory and readable, add path and name
            }elseif(is_dir($path) && is_readable($path)) {
                // recursive callback to open new directory
                $allData = scanDirectories_2($path, $allData);
            }
        }
    }
    return $allData;
}

/**
 * Checks if a folder exist and return canonicalized absolute pathname (sort version)
 * @param string $folder the path being checked.
 * @return mixed returns the canonicalized absolute pathname on success otherwise FALSE is returned
 */
function folder_exist($folder)
{
    // Get canonicalized absolute pathname
    $path = realpath($folder);

    // If it exist, check if it's a directory
    return ($path !== false AND is_dir($path)) ? $path : false;
}

function f_extension($fn)
{
	$str  = explode('/',$fn);
	$len  = count($str); 
	$str2 = explode('.',$str[($len-1)]); 
	$len2 = count($str2);
	$ext  = $str2[($len2-1)];
	return $ext;
}
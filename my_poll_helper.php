<?php

function ramdom_color()
{
    static $color_index = -1;
    static $bar_colors = array('#ff0000','#ff7f00','#ffff00','#7fff00','#00ff7f','#00bfbf','#007fff','#0000ff','#5f00bf','#bf00bf','#bf005f','#7f7f7f');
    
    if($color_index === -1)
    {
        shuffle($bar_colors);
        $color_index = rand(0, count($bar_colors));
    }
    
    $color_index++;
    if(!isset($bar_colors[$color_index])) $color_index = 0;
    return $bar_colors[$color_index];
}

function view_poll($id)
{
    $ci =& get_instance();
    $hide_results = false;
	$query_poll = $ci->db->query("SELECT * FROM poll WHERE id = ".$id." AND active = 1  LIMIT 1");// AND (start_date  != '' OR end_date  != '')
	$poll = $query_poll->row_array();;
	$query_poll->free_result();
 
	if($poll)
	{
		// Do we even allow the results to be seen?
		if($poll['show_results'] == 0)
		{
			echo '<div class="row"><div class="col-md-12"><div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Bạn không được phép xem kết quả bình chọn.</div></div></div>';
			$hide_results = true; // User is not allowed to see results
		}
		
		if(!empty($poll['end_date']))
		{
			// If we are hiding results when poll is finished
			if(strtotime($poll['end_date']) < time() && $poll['finished'] == 0)
			{
				echo '<div class="row"><div class="col-md-12"><div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Sự kiện bình chọn đã kết thúc.</div></div></div>';
				$hide_results = true; // User is not allowed to see results
			}
		} else
		{
			$ip = $_SERVER['REMOTE_ADDR'] ? $_SERVER['REMOTE_ADDR'] : false;
			// If poll requires a vote to see results check that
			if($ip && $poll['show_results_req_vote'] == 1)
			{
				
				$query_votes = $ci->db->query("SELECT COUNT(*) AS num_votes FROM poll_votes WHERE poll_id = {$id} AND ip = $ip");
				$result = $query_votes->result_array()[0];
				$query_votes->free_result();


				if($result['num_votes'] == 0)
				{
					echo '<div class="row"><div class="col-md-12"><div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Bạn đã bình chọn cho sự kiện này</div></div></div>';
					$hide_results = true; // User is not allowed to see results
				}
			}
		}

		if(!$hide_results)
		{
			// All the totals are made ahead of time during the vote process so we don't need to do any calculations from within MYSQL
			$query_poll_answers = $ci->db->query("SELECT * FROM poll_answers WHERE poll_id = ".$id." ORDER BY sort ASC");
	        $answers = $query_poll_answers->result_array();
	        $query_poll_answers->free_result();


			if($answers)
			{
				echo '<table class="table-responsive panel-collapse col-md-12">';
				echo '<tr><td colspan="2"><h3>'.$poll['question'].'</h3></td></tr>';
				foreach($answers as $answer)
				{
					echo '<tr><td class="poll_result_answer" colspan="2"><strong>'.$answer['answer'].'</strong></td></tr><tr>';
					
					if($poll['animate_results'] == 1)
					{
						 $pc = $poll['total_votes']>0?round($answer['num_votes']/$poll['total_votes']*100,$poll['result_precision']):0;
						echo '<td class="poll_bar"><div style="position:relative;"><div id="poll_result_bar'.$answer['id'].'" class="poll_result_bar" style="background-color:'.$answer['color'].';width: 0;"><div class="poll_result_shade">&nbsp;</div></div><div class="poll_result_stats">';
						echo '<div class="progress progress-striped active"> <div class="progress-bar progress-bar-success" style="width: '. $pc.'%"><span class="sr-only">'. $pc.'% Complete (success)</span> '.($poll['show_vote_numbers']==1?$answer['num_votes'].' - ':'').($poll['total_votes']>0?(round(($answer['num_votes'] / $poll['total_votes'] * 100), $poll['result_precision'])):'0').'%</div> </div>';
						echo '</div></div></td>';
					} else
					{
						echo '<td class="poll_bar"><div style="position:relative;"><div id="poll_result_bar'.$answer['id'].'" class="poll_result_bar" style="background-color:'.$answer['color'].';width: '.round((($answer['num_votes'] / $poll['total_votes']) * 100) ).'%;"><div class="poll_result_shade">&nbsp;</div></div><div class="poll_result_stats">'.($poll['show_vote_numbers']==1?$answer['num_votes'].' - ':'').round(($answer['num_votes'] / $poll['total_votes'] * 100), $poll['result_precision']).'%'.'</div></div></td>';
					}
					echo '</tr>';
				}
				echo '</table>';
			}
		}
	} else
	{
		echo '<div class="row"><div class="col-md-12"><div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Không tìm thấy sự kiện</div></div></div>';
	}
}

function load_poll($id,$current_date)
{
		$ci =& get_instance();
		 
		$query_poll = $ci->db->query("SELECT * FROM poll WHERE id = ".$id." AND removed = 0 AND active = 1 AND (start_date IS NULL OR start_date <= '".$current_date."') LIMIT 1");//  AND (start_date  != '' OR end_date  != '')  
		$data_poll  = $query_poll->row_array();
 
		$query_poll->free_result();

		if($data_poll && $data_poll != NULL)
		{
			$query_poll_answers = $ci->db->query("SELECT * FROM poll_answers WHERE poll_id = ".$id." ORDER BY sort ASC");
			$data_poll_answers = $query_poll_answers->result_array();
			$query_poll_answers->free_result();

			if(!empty($data_poll['end_date']) && (strtotime($data_poll['end_date']) < time()) && $data_poll['finished'] == 1)
			{
				view_poll($id);
			} elseif(!empty($data_poll['end_date']) && (strtotime($data_poll['end_date']) < time()) && $data_poll['finished'] == 0){
				echo '<div class="row"><div class="col-md-12"><div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Sự kiện bình chọn đã kết thúc.</div></div></div>';
			} else {

				if(count($data_poll_answers)>0)
				{

					//'load from submit poll';
					echo '<form class="alert alert-info fade in" id="vote_poll" name="vote_poll"><input type="hidden" name="id" value="'.$id.'" vote_poll_id="'.$id.'" />';
					echo '<table class=table-responsive panel-collapse col-md-12"><tr><td colspan="2"><h3>'.$data_poll['question'].'</h3></td></tr>';
					foreach($data_poll_answers as $answer)
					{
						echo '<tr><td>';
						if($data_poll['num_selections'] > 1)
						{
							echo '<div class="checkbox custom-checkbox"><input class="form-control" type="checkbox" name="answer[]" value="'.$answer['id'].'" id="show_results_'.$answer['id'].'" /><label for="show_results_'.$answer['id'].'">&nbsp;&nbsp;'.$answer['answer'].'</label></div>';
						} else
						{
							echo '<span class="radio custom-radio"><input type="radio" class="form-control" name="answer" id="show_results_'.$answer['id'].'" value="'.$answer['id'].'" /> &nbsp;&nbsp;<label for="show_results_'.$answer['id'].'">&nbsp;&nbsp;'.$answer['answer'].'</label></span>';
						}
						echo '</td><td></td></tr>';
					} 

					echo '<tr><td colspan="2"><p><a name="Submit" href="javascript:;" class="btn btn-default mb5 btn_vote">Vote</a></p></td></tr></table>';

					echo '</form><script type="text/javascript" src="'.base_url().'assets/themes/global/plugins/jquery.min.js"></script><script type="text/javascript" src="'.base_url().'assets/themes/adminre/javascript/backend/pages/poll_ajax.js"></script>';
				} else {
					// The poll ID they are passing in does not have any answers
					echo '<div class="row"><div class="col-md-12"><div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Sự kiện vẫn chưa có bình chọn nào</div></div></div>';
				}

			}
		}else{
			// The poll ID they are passing in does not exist
			echo '<div class="row"><div class="col-md-12"><div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Không tìm thấy sự kiện</div></div></div>';
		}
}


function poll_total_poll_votes_1($poll_id)
    {
    	$ci =& get_instance();
        $query = $ci->db->select('SUM(poll_answers.num_votes) AS total_votes') 
                 ->from('poll_answers')
                 ->join("poll", "poll.id = poll_answers.poll_id")
                 ->where("poll_answers.poll_id", intval($poll_id))
                 //->order_by('sort','desc')
                 ->get();
 
        $result =  $query->row_array();
        $query->free_result();
        return $result['total_votes'];
    }

function poll_total_poll_votes($poll_id)
    {
    	$ci =& get_instance();
        $query = $ci->db->select('count(poll_votes.id) AS total_votes') 
                 ->from('poll_votes')
                 ->join("poll", "poll.id = poll_votes.poll_id")
                 ->where("poll_votes.poll_id", intval($poll_id))
                 //->order_by('sort','desc')
                 ->get();
 
        $result =  $query->row_array();
        $query->free_result();
        return $result['total_votes'];
    }


function poll_answer_name($id)
{
    $ci =& get_instance();
    $ci->load->model('poll_answers');
    $ci->poll_answers->get_poll_answer_name($id);
}
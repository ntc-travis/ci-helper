<?php
function hit_onlining()
{
	$ci =&get_instance();
	$sql = $ci->db->query("SELECT COUNT(`id`) AS 'hit_onlining' FROM `hit_counter` WHERE DATE_FORMAT(FROM_UNIXTIME(`last_login`), '%H') <= 13 AND DATE( FROM_UNIXTIME(`last_login`)) = CURRENT_DATE()");
		if($sql->num_rows() > 0 ){
			$result = $sql->row_array()['hit_onlining'];
		}else{
			$result = 1;
		};
	return $result;
}

function member_online()
{
	$ci =&get_instance();
	$sql = $ci->db->query("SELECT SUM( IF( `user_id` <> '' , 1 , 0 ) ) AS 'member_online_in_day'
				FROM `hit_counter`
				WHERE  DATE( FROM_UNIXTIME(`last_login`)) = CURRENT_DATE()
				AND  `log_status` =  'login'");
		if($sql->num_rows() > 0 ){
			$result = $sql->row_array()['member_online_in_day'];
		}else{
			$result = 1;
		};
	return $result;
}

function visitor_online()
{
	$ci =&get_instance();
	$sql = $ci->db->query("SELECT SUM( IF( `user_id` = '' , 1 , 0 ) ) AS 'visitor_online_in_day'
				FROM `hit_counter`
				WHERE  DATE( FROM_UNIXTIME(`last_login`)) = CURRENT_DATE()");
		if($sql->num_rows() > 0 ){
			$result = $sql->row_array()['visitor_online_in_day'];
		}else{
			$result = 1;
		};
	return $result;
}

function hit_count_condition($num)
{
	$ci =&get_instance();

	$d_today   = date('Y-m-d');
	$d_lastday = last_datetime($d_today,$num,'day');

	$sql = $ci->db->query("SELECT COUNT(`id`) AS 'week' FROM `hit_counter`
							WHERE  DATE( FROM_UNIXTIME(`last_login`,'%Y-%m-%d')) BETWEEN '$d_lastday' AND '$d_today'");

	if($sql->num_rows() > 0 ){
		$result = $sql->row_array()['week'];
	}else{
		$result = 1;
	};
	return $result;
}

function hit_count_total()
{
	$ci =&get_instance();
	$sql = $ci->db->query("SELECT COUNT(`id`) AS 'total' FROM `hit_counter`");
		if($sql->num_rows() > 0 ){
			$result = $sql->row_array()['total'];
		}else{
			$result = 1;
		};
	return $result;
}

function hit_browser_total()
{
	$ci =&get_instance();
	$d_today   = date('Y-m-d');
	$d_lastday = last_datetime($d_today,$num=30,'day');
	$sql = $ci->db->query("SELECT `browser_name`, `platform`, `user_agent`,  COUNT(`id`) AS 'total' FROM `hit_counter`
	WHERE DATE( FROM_UNIXTIME(`last_login`,'%Y-%m-%d')) BETWEEN '$d_lastday' AND '$d_today'  GROUP BY `browser_name`");
	$result = $sql->result_array();
	return $result;
}

/*map: dasboard*/
function get_states($bool_status=FALSE,$switch='weeks')
{
	$ci      =&get_instance();
	$year    = date('Y');
	$d_today = date('Y-m-d');
	$data    ='';
	$where = ($bool_status === TRUE)? " AND  `user_id` !=''":" AND  `user_id` = ''";
	if($switch=='weeks'){
		$list_week = list_day_of_week($d_today);
		foreach ($list_week as $key => $value) {
			// D -> Tue
			// l -> Tuesday
			$sql = $ci->db->query("SELECT COUNT(`id`) AS 'week' FROM `hit_counter`
								WHERE  FROM_UNIXTIME(`last_login`,'%Y-%m-%d') ='$value' $where ");
			$data[date('D', strtotime( $value))] =  $sql->row_array()['week'];
		}

	}else{
		$list_month= list_month_of_year();
		foreach ($list_month as $key => $value) {
			if( $key == 0){
				unset($list_month[ $key]);
			}
		}
		foreach ($list_month as $key => $value) {
			$dtime = $year.'-'.$value;
			$sql = $ci->db->query("SELECT COUNT(`id`) AS 'month' FROM `hit_counter`
								WHERE  FROM_UNIXTIME(`last_login`,'%Y-%b') ='$dtime' $where");
			$data[$value] =  $sql->row_array()['month'];
		}
	}

	$ajax_map ='';
	$totals = 0;

	foreach ($data as $key => $value) {
		$ajax_map[]  = "['".$key."',".$value."]";
		$totals += $value;
	}
	$ajax_map = "[".implode(",",$ajax_map)."]";
	foreach ($data as $key => $value) {
		$return [] = [$key,$value];
	}
	$ajax_map = json_encode($return);
	return compact('data','totals','ajax_map');
}


function save_tracking()
{
	$ci =& get_instance();

	//$user_session_id = $ci->session->userdata('logged_in');

	$log_ip  = $_SERVER['REMOTE_ADDR'];
	$user_id = 0;
	$stamp   = time();
	$ua      = get_browsers();

	$ua_info = "Your browser: " . $ua['name'] . " " . $ua['version'] . " on " .$ua['platform'] . " reports: <br >" . $ua['user_agent'];

	$session_id = session_id();

	$browser_name = $ua['name'] . " v." . $ua['version'];
	$platform     = $ua['platform'];
	$user_agent   = $ua['user_agent'];
	$pattern     = $ua['pattern'];


	settype($user_id,"int");
	settype($stamp,"int");

	$sql = $ci->db->select('session_id')
 			->from('hit_counter')
 			->where('session_id',intval($session_id))
 			->get();

	if($_SERVER['QUERY_STRING']){
		$current_url = current_url().'?'.$_SERVER['QUERY_STRING'];
	}else{
		$current_url = current_url();
	}

	if ($sql->num_rows()>0 ){

			$data = array('log_ip'=>$log_ip,
						  'last_login'=>$stamp);
			$ci->db->where('session_id', $session_id);
			$ci->db->update('hit_counter', $data);

	} else {

	   $data = array('user_id'=>$user_id,
				'log_ip'     =>$log_ip,
				'browser_name' =>$browser_name,
				'platform' =>$platform,
				//'pattern' =>$pattern,
				'user_agent' =>$user_agent,
				'url_view' =>$current_url,
				'last_login' =>$stamp,
				'log_status' =>'view',
				'session_id' =>$session_id);
		$ci->db->insert('hit_counter',$data);

	}
} //cap nhat thong tin khi co khach ghe tham website

 //Get an array with geoip-infodata
/*function geoCheckIP($ip)
{
   //check, if the provided ip is valid
   if(!filter_var($ip, FILTER_VALIDATE_IP))
   {
           throw new InvalidArgumentException("IP is not valid");
   }

   //contact ip-server
   $response=@file_get_contents('http://www.netip.de/search?query='.$ip);
   if (empty($response))
   {
           throw new InvalidArgumentException("Error contacting Geo-IP-Server");
   }

   //Array containing all regex-patterns necessary to extract ip-geoinfo from page
   $patterns=array();
   $patterns["domain"] = '#Domain: (.*?)&nbsp;#i';
   $patterns["country"] = '#Country: (.*?)&nbsp;#i';
   $patterns["state"] = '#State/Region: (.*?)<br#i';
   $patterns["town"] = '#City: (.*?)<br#i';

   //Array where results will be stored
   $ipInfo=array();

   //check response from ipserver for above patterns
   foreach ($patterns as $key => $pattern)
   {
           //store the result in array
           $ipInfo[$key] = preg_match($pattern,$response,$value) && !empty($value[1]) ? $value[1] : 'not found';
   }

   return $ipInfo;
}*/

function get_browsers()
{

	$u_agent  = $_SERVER['HTTP_USER_AGENT'];
	$bname    = 'Unknown';
	$platform = 'Unknown';
	$version  = "";

	//First get the platform?
	if (preg_match('/linux/i', $u_agent)) {
		$platform = 'linux';
	}
	elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
		$platform = 'mac';
	}
	elseif (preg_match('/windows|win32/i', $u_agent)) {
		$platform = 'windows';
	}

	// Next get the name of the user_agent yes seperately and for good reason
	if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
	{
		$bname = 'Internet Explorer';
		$ub = "MSIE";
	}
	elseif(preg_match('/Firefox/i',$u_agent))
	{
		$bname = 'Mozilla Firefox';
		$ub = "Firefox";
	}
	elseif(preg_match('/Chrome/i',$u_agent))
	{
		$bname = 'Google Chrome';
		$ub = "Chrome";
	}
	elseif(preg_match('/Safari/i',$u_agent))
	{
		$bname = 'Apple Safari';
		$ub = "Safari";
	}
	elseif(preg_match('/Opera/i',$u_agent))
	{
		$bname = 'Opera';
		$ub = "Opera";
	}
	elseif(preg_match('/Netscape/i',$u_agent))
	{
		$bname = 'Netscape Navigator';
		$ub = "Netscape";
	}
	elseif(preg_match('/Seamonkey/i',$u_agent))
	{
		$bname = 'Seamonkey';
		$ub = "Seamonkey";
	}
	elseif(preg_match('/Konqueror/i',$u_agent))
	{
		$bname = 'Konqueror';
		$ub = "Konqueror";
	}
	elseif(preg_match('/Gecko/i',$u_agent))
	{
		$bname = 'Gecko';
		$ub = "Gecko";
	}
	elseif(preg_match('/Avant/i',$u_agent))
	{
		$bname = ' Avant Browser';
		$ub = "Avant";
	}
	elseif(preg_match('/Camino/i',$u_agent))
	{
		$bname = 'Camino';
		$ub = "Camino";
	}
	elseif(preg_match('/Flock/i',$u_agent))
	{
		$bname = 'Flock';
		$ub = "Flock";
	}
	elseif(preg_match('/Aol/i',$u_agent))
	{
		$bname = 'Aol';
		$ub = "Aol";
	}

	elseif(preg_match('/Mosaic/i',$u_agent))
	{
		$bname = 'Mosaic';
		$ub = "Mosaic";
	}
	elseif(preg_match('/Lynx/i',$u_agent))
	{
		$bname = 'Lynx';
		$ub = "Lynx";
	}
	elseif(preg_match('/Amaya/i',$u_agent))
	{
		$bname = 'Amaya';
		$ub = "Amaya";
	}
	elseif(preg_match('/Omniweb/i',$u_agent))
	{
		$bname = 'Omniweb';
		$ub = "Omniweb";
	}


	// finally get the correct version number
	$known = array('Version', $ub, 'other');
	$pattern = '#(?<browser>' . join('|', $known) . ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';

	if (!preg_match_all($pattern, $u_agent, $matches)) {
		// we have no matching number just continue
	}

	// see how many we have
	$i = count($matches['browser']);
	if ($i != 1) {
		//we will have two since we are not using 'other' argument yet
		//see if version is before or after the name
		if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
			$version= (isset($matches['version'][0])==true)?$matches['version'][0]:'0';
		}
		else {
			$version= (isset($matches['version'][1])==true)?$matches['version'][1]:'1';
		}
	}
	else {
		$version= $matches['version'][0];
	}

	// check if we have a number
	if ($version==null || $version=="") {$version="?";}

	return array(
		'user_agent' => $u_agent,
		'name'      => $bname,
		'version'   => $version,
		'platform'  => $platform,
		'pattern'    => $pattern
	);
}
<?php

function img_exit($product_img)
{	
	if(file_exists(FCPATH.$product_img)  && http_response(base_url().$product_img)['reponse_code'] == '200')
	{
		return base_url().$product_img;
	}else{

		return base_url().'assets/themes/no_image.jpg';
	}
}


/*
 * @param string $file Filepath
 * @param string $query Needed information (0 = width, 1 = height, 2 = mime-type)
 * @return string Fileinfo
 */
function getImageinfo($file, $query) {
	if (!realpath($file)) {
	       $file = $_SERVER["DOCUMENT_ROOT"].$file;
	}
	$image = getimagesize($file);
   return $image[$query];
}
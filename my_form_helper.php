<?php


if ( ! function_exists('validation_array_errors'))
{
	function validation_array_errors($prefix = '', $suffix = '')
	{
		if (FALSE === ($OBJ =& _get_validation_object()))
		{
			return '';
		}

		return $OBJ->error_string($prefix, $suffix, true);
	}
}

/**
 * プルダウン用に配列を生成する。
 * @param $str デフォルト表示の文字。指定すると一番上の0要素になる。
 *
 **/
function options($data, $str = null)
{
	$key_s = array_keys($data);

	if(!is_null($str)) {
		array_unshift($key_s, '');
		array_unshift($data, $str);
	}

	return array_combine($key_s, $data);
}



/**
 * 第一引数にフィールドを渡すと出力とhiddenを作成する
 * 第二引数に配列を渡して対応する文字を出力する
 * @param $field
 * @param bool $array
 * @return string
 */
function confirm($field, $array = false, $default = '')
{
	$value = (isset($_POST[$field])) ? $_POST[$field]: null;
	if((is_null($value) || $value === '') && func_num_args() === 3) {
		return $default;
	}elseif(is_array($value)) {

	}

	if(is_array($array)) {
		if(isset($array[$value])) return e($array[$value]).form_hidden($field, $value);
		return $default;
	}

	return e($value).form_hidden($field, $value);
}


function number_confirm($field, $decimal = 0)
{
	if(!isset($_POST[$field]) || $_POST[$field] === '') return '';
	$value = $_POST[$field];

	return e(number_format($value, $decimal)).form_hidden($field, $value);
}

function date_confirm($format, $field, $default = '')
{
	$value = $_POST[$field];
	if(!strtotime($value) || !$value) {
		return $default.form_hidden($field, '');
	}

	return date($format, strtotime($value)).form_hidden($field, $value);
}

/**
 * get用
 */
function checked($field, $value)
{
	$get = (isset($_GET[$field]))? $_GET[$field]: '';
	if(is_array($get) && in_array($value, $get)) {
		return 'checked';
	}elseif($get !== '' && $get == $value) {
		return 'checked';
	}
	return '';
}

function form_token()
{
	$token = get_instance()->session->set_token();
	return form_hidden('token', $token);
}

// Checks if string is a URL
  // @param string $url
  // @return bool
function is_valid_url($url) {
    // First check: is the url just a domain name? (allow a slash at the end)
    $_domain_regex = "|^[A-Za-z0-9-]+(\.[A-Za-z0-9-]+)*(\.[A-Za-z]{2,})/?$|";
    if (preg_match($_domain_regex, $url)) {
        return true;
    }

    // Second: Check if it's a url with a scheme and all
    $_regex = '#^([a-z][\w-]+:(?:/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))$#';
    if (preg_match($_regex, $url, $matches)) {
        // pull out the domain name, and make sure that the domain is valid.
        $_parts = parse_url($url);
        if (!in_array($_parts['scheme'], array( 'http', 'https' )))
            return false;

        // Check the domain using the regex, stops domains like "-example.com" passing through
        if (!preg_match($_domain_regex, $_parts['host']))
            return false;

        // This domain looks pretty valid. Only way to check it now is to download it!
        return true;
    }

    return false;
}


function posts($field_name,$value_name)
{
    if(isset($_POST[$field_name])){
    	return $_POST[$field_name];
    }
    if(!isset($_POST[$field_name]) || $_POST[$field_name] === ''){
        return $value_name;
    }
}
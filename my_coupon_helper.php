<?php
function hp_coupon_category_list( )
{
	$ci =& get_instance();
	$fasle = false;
	$query = $ci->db->select('*')
		->from('categories')
		->where(array("categories.category_status"=>1,"categories.category_sub"=>1))
        ->get();
	$result =  $query->result_array();
	$query->free_result();
    if($result){
    	return $result;
    }
 	return $fasle;
}
function hp_coupon_color_list( )
{
	$ci =& get_instance();
	$fasle = false;
	$query = $ci->db->select('*')
		->from('colors')
		->like("colors.color_status",1)
        ->get();
	$result =  $query->result_array();
	$query->free_result();
    if($result){
    	return $result;
    }
 	return $fasle;
}
function hp_coupon_size_list( )
{
	$ci =& get_instance();
	$fasle = false;
	$query = $ci->db->select('*')
		->from('sizes')
		->like("sizes.size_status",1)
        ->get();
	$result =  $query->result_array();
	$query->free_result();
    if($result){
    	return $result;
    }
 	return $fasle;
}
function hp_coupon_site_list( )
{
	$ci =& get_instance();
	$fasle = false;
	$query = $ci->db->select('*')
		->from('sites')
        ->get();
	$result =  $query->result_array();
	$query->free_result();
    if($result){
    	return $result;
    }
 	return $fasle;
}
function hp_coupon_count_by($category_id,$site_id,$product_category_name,$status)
{
	$ci =& get_instance();
	if($category_id){
		$ci->db->where("products.category_id",$category_id);
	}
	if($site_id){
		$ci->db->where("products.site_id",$site_id);
	}
	if($product_category_name){
		//$ci->db->like("products.product_category_name",$product_category_name);
		$product_category_name = hp_convert_str($product_category_name,$format='');
		$ci->db->where("(TRIM(LOWER(products.product_category_name)) LIKE '%{$product_category_name}%')");
	}
	if($status !=''){
		$ci->db->where("products.product_status",$status); //1:show; 2:hide; 3:block
	}
 	$query = $ci->db->select('count(*) AS ct') 
 		   ->from('products')
           ->join("categories", "categories.id = products.category_id")
           ->join("sites", "sites.id = products.site_id")
           ->get();
	$result =  $query->result_array();
	$query->free_result();
	if($result){
		return $result;
	}else{
		return false;
	}
	return false;
}

function hp_coupon_count_product_by_search($product_name,$category_name)
{
	$ci =& get_instance();

    if(!empty($product_name)){
    	 //codeigniter  2
         //$ci->db->like('TRIM(LOWER(products.product_name))',hp_convert_str($product_name,$format=''),'both');
         //
         //codeigniter  3
         //$product_name = hp_convert_str($product_name,$format='');
         $ci->db->where("(TRIM(LOWER(products.product_name)) LIKE '%{$product_name}%')");
    }
    if(!empty($category_name)){
         //$ci->db->like('TRIM(LOWER(products.product_category_name))',hp_convert_str($category_name,$format=''),'both');
         //$category_name = hp_convert_str($category_name,$format='');
         $ci->db->where("(TRIM(LOWER(products.product_category_name)) LIKE '%{$category_name}%')");
    }

 	$query = $ci->db->select('count(products.id) AS ct')
 		   ->from('products')
           ->join("categories", "categories.id = products.category_id")
           ->join("sites", "sites.id = products.site_id")
           ->get();
	$result =  $query->result_array();
	//d($ci->db->last_query());
	
	//d($result);
	if($result){
		return $result;
	}else{
		return false;
	}
	$query->free_result();
	return false;
}


function hp_coupon_category_sub_list($site_id)
{
	$ci =& get_instance();
	if(!empty($site_id)){
		$ci->db->where("products.site_id",$site_id);
	}
	$query = $ci->db->select('products.id AS product_id,products.site_id,products.category_id,products.product_category_name')
		->from('products')
		->join("categories", "categories.id = products.category_id")
		->join("sites", "sites.id = products.site_id")
        ->where("products.product_status",1)
        ->group_by("products.product_category_name")
        ->order_by("products.id","desc")
        ->get();
 	$result =  $query->result_array();
	$query->free_result();
	return $result;
 }

 function hp_coupon_color_sub_list($site_id='')
{
	$ci =& get_instance();
	if(!empty($site_id)){
		$ci->db->where("products.site_id",$site_id);
	}
	$query = $ci->db->select('products.product_color')
		->from('products')
		->join("categories", "categories.id = products.category_id")
		->join("sites", "sites.id = products.site_id")
        ->where("products.product_status",1)
        ->group_by("products.product_color")
        ->order_by("products.id","desc")
        ->get();
 	$result =  $query->result_array();
	$query->free_result();
	return $result;
 }
 function hp_coupon_size_sub_list($site_id='')
{
	$ci =& get_instance();
	if(!empty($site_id)){
		$ci->db->where("products.site_id",$site_id);
	}
	$query = $ci->db->select('products.product_size')
		->from('products')
		->join("categories", "categories.id = products.category_id")
		->join("sites", "sites.id = products.site_id")
        ->where("products.product_status",1)
        ->group_by("products.product_size")
        ->order_by("products.id","desc")
        ->get();
 	$result =  $query->result_array();
	$query->free_result();
	return $result;
 }
function hp_coupon_product_vote_id($id)
{
	$ci =& get_instance();
	$ci->load->model('Product_votes');
	$product_rating_id = $ci->Product_votes->get_coupon_rating($id);
	return $product_rating_id;
}
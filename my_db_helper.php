<?php
function enum_select( $table = '', $field = ''){

	$enums = array();
    if ($table == '' || $field == '') return $enums;
	$ci =& get_instance();
    $query = " SHOW COLUMNS FROM `$table` LIKE '$field' ";
    $row = $ci->db->query(" SHOW COLUMNS FROM `$table` LIKE '$field' ")->row()->Type;
    $regex = "/'(.*?)'/";
    preg_match_all( $regex , $row, $enum_array );
    $enums = $enum_array[1];
    return( $enums );
}

/*CONTACT*/
function count_contact($contact_mailbox = '')
{
	$ci =& get_instance();

	if($contact_mailbox != ''){
		$ci->db->where('contact_mailbox',intval($contact_mailbox));
	}


	$query = $ci->db->select('count(*) AS ct')
 			->from('contact')
 			->join('contact_job','contact_job.id = contact.contact_job_id','left')
 			//->join('contact_tag','contact_tag.id = contact.contact_tag_id','left')
 			->where('contact_job.contact_job_status',1)
 			//->where('contact_tag.contact_tag_status',1)
 			->get();

	$result =  $query->result_array()[0]['ct'];
	$query->free_result();
	return $result;
}

function count_contact_tag($contact_tag_id = '')
{
	$ci =& get_instance();

	if($contact_tag_id != ''){
		$ci->db->where('contact_tag_id',intval($contact_tag_id));
	}


	$query = $ci->db->select('count(*) AS ct')
 			->from('contact')
 			->join('contact_job','contact_job.id = contact.contact_job_id','left')
 			//->join('contact_tag','contact_tag.id = contact.contact_tag_id','left')
 			->where('contact_job.contact_job_status',1)
 			//->where('contact_tag.contact_tag_status',1)
 			->get();

	$result =  $query->result_array()[0]['ct'];
	$query->free_result();
	return $result;
}


function count_contact_important($contact_important = '')
{
	$ci =& get_instance();

	if($contact_important != ''){
		$ci->db->where('contact_important',intval($contact_important));
	}


	$query = $ci->db->select('count(*) AS ct')
 			->from('contact')
 			->join('contact_job','contact_job.id = contact.contact_job_id','left')
 			//->join('contact_tag','contact_tag.id = contact.contact_tag_id','left')
 			//->where('contact_job.contact_job_status',1)
 			//->where('contact_tag.contact_tag_status',1)
 			->get();

	$result =  $query->result_array()[0]['ct'];
	$query->free_result();
	return $result;
}


/*function count_contact_with_condition($contact_mailbox, $dtime = '')
{
	$ci =& get_instance();

	if($contact_mailbox == 1){
		$ci->db->where('contact_status',1); //1:no read; 2: read
	}

	if($dtime != ''){
		$ci->db->where('created_at',$dtime); //1:no read; 2: read
	}


		$query = $ci->db->select('count(*) AS ct')
	 			->from('contact')
	 			->where('contact_mailbox',intval($contact_mailbox))
	 			->get();

	$result =  $query->result_array()[0]['ct'];
	$query->free_result();
	return $result;
}*/

function update_contact_status($arr_data)
{
	$ci =& get_instance();
	$ci->load->model('Contacts');
	$ci->Contacts->auto_update($arr_data);
}

function calc_contact_space_used($contact_mailbox)
{
	//$ci =& get_instance();
	$total = count_contact()>0?count_contact():1;
	$total_of_mailbox = count_contact($contact_mailbox);

	$percent = ($total_of_mailbox/$total )*100;
	 
	return $percent .'%';
}
/*#########################################################################*/




/*#########################################################################*/
/*PRODUCT VOTE*/


/*END PRODUCT VOTE*/
/*#########################################################################*/


/*MEMBER*/
function count_member_status($status)
{
	$ci =& get_instance();

	if($status != '' || $status >= 0){
		$ci->db->where('member_active',intval($status)); //0:unactive; 1:active; 2: block
	}

	$query = $ci->db->select('count(*) AS ct')
 			->from('member_infos')
 			->where('member_status','user')
 			->get();

	$result =  $query->result_array()[0]['ct'];
	$query->free_result();
	return $result;
}


function count_member_by_search($search)
{
	$ci =& get_instance();
 	$query = $ci->db->select('count(*) AS ct') 
					->from('member_infos')
					->where('member_status','user')
					->group_start()
					->or_like($search,"both")
					->group_end()
					->get();

	$result =  $query->result_array();
	$query->free_result();
	if($result){
		return $result;
	}else{
		return false;
	}
	return false;
}

/*SIDEBAR LEFT*/

function get_list_category_name($id)
{
	$ci =& get_instance();
	$query = $ci->db->select('categorys.category_name') 
		->from('categorys')
        ->where(array("categorys.category_status"=>1,"categorys.id"=>$id))
        ->get();

 	$result =  $query->result_array();

	$query->free_result();
	return $result[0]['category_name'];
}


function get_list_category_group_name($id)
{
	$ci =& get_instance();
	$query = $ci->db->select('category_groups.category_group_name,category_groups.category_group_url_param') 
		->from('category_groups')
        ->where(array("category_groups.category_group_status"=>1,"category_groups.id"=>$id))
        ->get();

 	$result =  $query->result_array();
	$query->free_result();
	//dd($result[0]);
	return $result[0];
}


function get_list_categorys_status()
{
	$ci =& get_instance();
	$query = $ci->db->select('categorys.id AS category_id, categorys.category_name') 
		->from('categorys')
        ->where("categorys.category_status",1)
        ->order_by("categorys.id","desc")
        ->get();

 	$result =  $query->result_array();
	$query->free_result();
	return $result;
}

function get_list_category_groups_status($category_id)
{
	$ci =& get_instance();
	if($category_id){
		$ci->db->where("category_groups.category_id",$category_id);
	}	
	$query = $ci->db->select('category_groups.id AS category_groups_id, 
		category_groups.category_id AS category_id, 
		category_groups.category_group_url_param AS category_group_param,
		category_groups.category_group_name') 
		->from('category_groups')
        ->where("category_groups.category_group_status",1)
        ->order_by("category_groups.id","desc")
        ->get();

 	$result =  $query->result_array();
	$query->free_result();
	return $result;
 }

function count_categorys($category_id)
{
	/*$ci =& get_instance();
	if($category_id){
		$ci->db->where("category_groups.category_id",$category_id);
	}
	$query = $ci->db->select('count(*) AS ct') 
		->from('category_groups')
		->join("categorys", "categorys.id = category_groups.category_id")
        ->where("category_groups.category_group_status",1)
        ->order_by("category_groups.id","desc")
        ->get();

 	$result =  $query->result_array();
	$query->free_result();
	return $result;*/
 }



function count_categorys_group($category_id)
{
	$ci =& get_instance();
	if($category_id){
		$ci->db->where("category_groups.category_id",$category_id);
	}

	$query = $ci->db->select('count(*) AS ct') 
		->from('category_groups')
		->join("categorys", "categorys.id = category_groups.category_id")
        ->where("category_groups.category_group_status",1)
        ->order_by("category_groups.id","desc")
        ->get();

 	$result =  $query->result_array();
	$query->free_result();
	return $result;
 }


function count_product_by_categoty($category_id,$category_group_id=null)
{
	$ci =& get_instance();
	if($category_id){
		$ci->db->where("product_infos.category_id",$category_id);
	}
	if($category_group_id !=''){
		$ci->db->where("product_infos.category_group_id",$category_group_id);
	}
 	$query = $ci->db->select('count(*) AS ct') 
 		   ->from('product_infos')
           ->join("categorys", "categorys.id = product_infos.category_id")
           ->join("category_groups", "category_groups.id = product_infos.category_group_id")
           ->join("category_animals", "category_animals.id = product_infos.category_animal_id")
           ->get();
	$result =  $query->result_array();
	$query->free_result();
	if($result){
		return $result;
	}else{
		return false;
	}
	return false;
}


function count_product_by_filter($filter)
{
	$ci =& get_instance();
	if($filter){
		$ci->db->where("product_infos.product_info_".$filter,1);
	}

 	$query = $ci->db->select('count(*) AS ct') 
 		   ->from('product_infos')
           ->join("categorys", "categorys.id = product_infos.category_id")
           ->join("category_groups", "category_groups.id = product_infos.category_group_id")
           ->join("category_animals", "category_animals.id = product_infos.category_animal_id")
           ->get();
	$result =  $query->result_array();
	$query->free_result();
	if($result){
		return $result;
	}else{
		return false;
	}
	return false;
}



function count_product_by_search($search)
{
	$ci =& get_instance();
	if($search){
		$ci->db->like("product_infos.product_info_name",$search,"both'");
	}

 	$query = $ci->db->select('count(*) AS ct') 
 		   ->from('product_infos')
           ->join("categorys", "categorys.id = product_infos.category_id")
           ->join("category_groups", "category_groups.id = product_infos.category_group_id")
           ->join("category_animals", "category_animals.id = product_infos.category_animal_id")
           ->get();
	$result =  $query->result_array();
	$query->free_result();
	if($result){
		return $result;
	}else{
		return false;
	}
	return false;
}

function count_product_status($status='')
{
	$ci =& get_instance();

	if($status !=''){
		$ci->db->where("product_infos.product_info_status",$status); //1:show; 2:hide; 3:block
	}

 	$query = $ci->db->select('count(*) AS product_status') 
 		   ->from('product_infos')
           ->join("categorys", "categorys.id = product_infos.category_id")
           ->join("category_groups", "category_groups.id = product_infos.category_group_id")
           ->join("category_animals", "category_animals.id = product_infos.category_animal_id")
           ->get();
	$result =  $query->result_array();
	$query->free_result();
	if($result){
		return $result;
	}else{
		return false;
	}
	return false;
}


/*=================================================*/
/*NEWS*/
function count_news_info_status($status='')
{
	$ci =& get_instance();

	if($status !=''){
		$ci->db->where("news_info.news_info_status",$status); //1:show; 2:hide;
	}

 	$query = $ci->db->select('count(*) AS ct') 
 		   ->from('news_info')
           ->join("news_category", "news_category.id = news_info.news_category_id")
           ->join("news_kind", "news_kind.id = news_info.news_kind_id")
           ->get();
	$result =  $query->result_array()[0]['ct'];
	$query->free_result();

	if($result){
		return $result;
	}else{
		return false;
	}
	return false;
}


function update_news_view($data)
{
	$ci =& get_instance();
	$ci->load->model('News_info');

	$result = $ci->News_info->check_news_info_by_id($data);
	$views ='';

	if($result['id'] == $data['id']){
		$views = $result['news_info_view']+1;
	}
 
  	$arr_data =  array('id' => $result['id'],'news_info_view' => $views);
	
	return $ci->News_info->auto_update($arr_data);
}



/*END NEWS*/
/*=================================================*/

function count_all($table_name)
{
    $ci =& get_instance();
    $ci->load->model($table_name);
	 return  $ci->db->count_all($table_name);
}


function count_status($table_name)
{
    $ci =& get_instance();
    $ci->load->model($table_name);
	 return  $ci->$table_name->count_where(1);
}

function get_prefecture($id)
{
    /*$ci =& get_instance();
    $ci->load->model('school');
    $ci->school->get_game_by_id($id);*/
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


	function clean($str) 
	{
		$str = trim($str);
		return stripslashes($str);
	}

	function csrf_token()
	{
		$ci =& get_instance();
		$token = md5(uniqid(rand(),true));
		$ci->session->set_userdata('token',$token);
		return $token;
	}

	function encode_password($password)
	{
		$salt = PASSWORD_SALT;
		$_pass = str_split($password);
		foreach ($_pass as $_hashpass)
		{
			$salt .= md5($_hashpass);
		}
		return md5($salt);
	}
	

	function vertify_admin_logged()
	{
		$ci = & get_instance(); 
		$is_logged_in = $ci->session->userdata('a_logged');
		if($is_logged_in)
		{
			return TRUE;
		}
		return FALSE;  
	}

	function vertify_user_logged()
	{
		$ci = & get_instance(); 
		$is_logged_in = $ci->session->userdata('m_logged');
		if($is_logged_in)
		{
			return TRUE;
		}
		return FALSE;  
	}

	function get_session_userdata($session_value, $session_userdata_name)
	{
		$ci = & get_instance(); 
		$is_logged_in = $ci->session->userdata($session_userdata_name);
		if($is_logged_in)
		{
			return $is_logged_in[$session_value];
		}
	}

	function safeEmail($string)
	{
	return  preg_replace( '((?:\n|\r|\t|%0A|%0D|%08|%09)+)i' , '', $string );
	}	
